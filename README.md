# README #

This Application serves the following requirements...

This application will have option for user to register themselves by providing First Name, Last Name, Email ID and Password.

* Application provides login screen to enter into application using email and password entered during registration.
* Once logged into the application User can, Add new Address, Edit/Modify existing address and delete the address from the application.
* For adding a new address following fields taken from user
	* Mandatory Fields
		* Name of the Addressee. (First Name, Last Name)
		* Type of Address : Residential, Office, Others
		* Address (Multi-line Text)
		* Mobile Number.
	* Optional Fields (for future integration with Maps)
		* Longitude
		* Latitude
* Address enter by one User can be seen/edited/deleted only by the same user. (PII Security concern) 
	* In other words, Users will not have access to addresses for which they are not the actual owners.
* Address once deleted will be completely removed from the application.

### Other requirements ###

* Log all SQL statements. Do not use logger
	* SQL Middleware is written as SqlDebugApp. 
	* This will be enabled/disabled using flag named "SQL_DEBUG" environmental variable. See .env under AddressAPI for this flag.
	
* Error handling
	* For every http method via GET, POST, PUT and DELETE, errors have been handled and right Response is sent to clients.
	* Have written a custom exception handling class under /utils/exceptionhandler.py

* Add good functionality of Django and/or Python
	* Pre-User Auth module is used for Token based authentication.
	* All APIs for CRUD operation are protected using auth token.
	* Have used Custom method to Obtain the Auth Token.
	* Customized error handling is also implemented.

* Make the project reproducible to another developer 
	* See Deployment instructions below
	
* server deployable. : 
	* Has been uploaded into dokerub also with the following tags
	* gururajkashikar/address_book:lastest
	* gururajkashikar/address_book:backend_lastest
	
	* docker-compose pull will pull these images from remote (if not already in server) and performs required things.
	
* Provide instructions on how one would start using your application : 
	* See Deployment instructions below



## How do I get set up? ##

###### Summary of set up ######
	* The setup for the address book has the following component.
	* Django backend exposing the CRUD APIs with Gunicorn WSGI server on Docker.
	* Angular application talking to backend and provided the User Interface for interation.
	
###### Configuration ######
	* This setup is made available as a two docker containers. one for Frontend with NGINX and other one 
	* with DJango application. NGIX acts as both reverse Proxy and Proxy loadbalancing server. Both Frontend and Backend sits behind 
	* NGINX. NGINX hides the ports exposed by both the apps by proxying the address for these apps.
	
######  Dependencies ######
	* Python-3.10.x.
	* Django 3.4.
	* Angular 10.
	* Docker Runtime.
	* Docker Compose.
	
######  Database configuration ######
	* SQLite DB is used along with Django application. No special configuration is needed.

###### Deployment instructions ######
	*Simple Steps.
		* Open favorite Command Line Interface (CLI) like Command Prompt or Linux Terminal. 
		* Clone the repo to some folder on your machine uisng "git clone https://<user-name>@bitbucket.org/gururaj-kashikar/doverassignment.git"
		* Change Directory (cd) doverassignment. Make sure docker-compose.yml is in the current directory
		* Run "docker-compose up". This will build all the required images, and at last prints "Booting worker with pid:XX."
		* Open your favorite browser and type http://localhost/.
		* You should be greeted with Login page. 
		* If this the the first time you are working with this application "Register" new user. 
	
	*Note: Username and Password from Register Page will be used for authentication.
		
		
		

