﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable  } from 'rxjs';
import { Address } from '../_models';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class AddressService {
    constructor(private http: HttpClient) { }

    getAll(id: number): Observable<Address[]> {
        return this.http.get<Address[]>(`${environment.apiUrl}/address/${id}`);
    }

    register(address: any) {
        return this.http.post(`${environment.apiUrl}/address/`, address);
    }

    update(address:string) {
        return this.http.put(`${environment.apiUrl}/address/`, address);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/address/${id}`);
    }
}