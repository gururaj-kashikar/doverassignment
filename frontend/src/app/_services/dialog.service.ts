import { ElementRef, Injectable } from '@angular/core'
import { MatDialog, MatDialogRef } from '@angular/material/dialog'

import { DialogBodyComponent } from '../dialog-body/dialog-body.component'


/**
 * Service to create modal dialog windows.
 */
@Injectable({ providedIn: 'root' })
export class DialogService {

  constructor(public dialog: MatDialog) { }

  public openDialog(
     positionRelativeToElement: ElementRef, hasBackdrop?: boolean,
      height?: string, width?: string
    ): MatDialogRef<DialogBodyComponent> 
    {
      if(!height) 
       height = "135px"
      if(!width)
       width = "290px" 
      const dialogRef: MatDialogRef<DialogBodyComponent> =
      this.dialog.open(DialogBodyComponent, {
        hasBackdrop: hasBackdrop,
        height: height,
        width: width,
        data: { positionRelativeToElement: positionRelativeToElement }
      })
    return dialogRef
  }

   
}