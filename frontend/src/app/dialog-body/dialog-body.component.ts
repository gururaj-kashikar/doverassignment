import { Component, OnInit, ElementRef, Inject } from '@angular/core';
import { MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA  } from "@angular/material/dialog";

@Component({
   templateUrl: './dialog-body.component.html',
})
export class DialogBodyComponent implements OnInit {

  private positionRelativeToElement: ElementRef

  constructor(public dialogRef: MatDialogRef<DialogBodyComponent>,  @Inject(MAT_DIALOG_DATA) public options: { positionRelativeToElement: ElementRef }) {
    this.positionRelativeToElement = options.positionRelativeToElement
   }

  ngOnInit(): void {
    const matDialogConfig = new MatDialogConfig()
    const rect: DOMRect = this.positionRelativeToElement.nativeElement.getBoundingClientRect()

    //matDialogConfig.position = { right: `10px`, top: `${rect.bottom + 2}px` }
    //this.dialogRef.updatePosition(matDialogConfig.position)
  }

  close(){
    this.dialogRef.close();
  }

}
