﻿import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Address, User } from '../_models';
import { UserService, AlertService, AddressService, AuthenticationService, DialogService } from '../_services';
import {MatDialog  } from '@angular/material/dialog';
import { DialogBodyComponent } from '../dialog-body/dialog-body.component';
import { OnInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core'

@Component({ templateUrl: 'home.component.html',
styleUrls: ['./style.scss'] })
export class HomeComponent implements OnInit {
   address : Address[] = [];
   dataJson: any = {};
   isEditAddNew:boolean = false;
   addressForm: FormGroup | undefined;
   results:string = "";
   loading = false;
   submitted = false;
   editAddButton:string;
   methodName:string;

   currentUserSubscription: Subscription;
   @ViewChild('newAddButton', { static: false }) public newAddButton: ElementRef
    dialogRef;

   

    constructor(
        private authenticationService: AuthenticationService,
        private addressService: AddressService,
        private userService: UserService,
        private matDialog: MatDialog,
        public dialogService: DialogService,
        private formBuilder: FormBuilder,
        private alertService: AlertService
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(data => {
            this.dataJson = data;
       });
    }

     ngOnInit() {
        this.loadAllAddress();
        this.addressForm = this.formBuilder.group({
            addressee: ['', Validators.required],
            addtype: ['', Validators.required],
            mobile: ['', Validators.required],
            address: ['', Validators.required],
            longitude: [''],
            latitude: [''],
            addid:[''],
            userid:[''],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.addressForm?.controls; }

    confrimDelete(name: string, id:number) {
        if(confirm("Are you sure to delete "+name)) {
          //console.log("Implement delete functionality here");
          this.addressService.delete(id)
          .pipe(first())
          .subscribe(() => {
              this.alertService.success("Address of " +name+" was deleted", true);
              this.loadAllAddress();
              setTimeout(()=>{ this.alertService.clear();}, 3000);
        })

      }
    }

   
    
    /***
     * Listing all the Address.
     */
    private loadAllAddress() {
        if(this.dataJson.user)
        {
          this.addressService.getAll(this.dataJson.user.id)
            .pipe(first())
            .subscribe(addr => {
                this.address = addr;
                //console.log("LoadAllAddress Address : ", addr);
            } );
        }
    }


    /***
     * 
     */
     addNewAdd(){
        for (const field in this.addressForm.controls) { // 'field' is a string
            this.addressForm.controls[field].reset();
            this.addressForm.controls[field].setErrors(null);
        }
        this.methodName="save";
        this.editAddButton = "Add";
        this. isEditAddNew = true;
     }

    /**
     * Edits the Seleted Address
     */
    edit()
    {
        this.submitted = true;
        // reset alerts on submit
       this.alertService.clear();
      
        // stop here if form is invalid
        if (this.addressForm?.invalid) {
            return;
        }
      
        let data = {};
        let id:number;
        //data["userid"] = this.dataJson.user.id;
        for (const field in this.addressForm.controls) { // 'field' is a string
               data[field] = this.addressForm.controls[field].value;
         }

       //console.log("Edited Address : ",JSON.stringify(data));
       this.addressService.update(JSON.stringify(data))
           .pipe(first())
           .subscribe(
               data => {
                   this.results = "Updated Successfully!";
                   this.alertService.success('Address is Updated', true);
                   this.loadAllAddress();
                   for (const field in this.addressForm.controls) { // 'field' is a string
                       this.addressForm.controls[field].reset();
                       this.addressForm.controls[field].setErrors(null);
                   }
                   setTimeout(()=>{ 
                    this.alertService.clear();
                    this.results = null;
                    this. isEditAddNew = false;
                    this.methodName = null;
                    this.editAddButton = null;
                   }, 2000);
                   
                   
               },
               error => {
                   this.alertService.error(error);
                   this.loading = false;
               });
    }
    

    showEditWindow(index){
        this.methodName="edit";
        this.editAddButton = "Edit";
        this.isEditAddNew = true;
        let editingAddress = {};
        editingAddress  =  Object.assign({}, this.address[index]);
        console.log("editingAddress ", editingAddress);
        this.addressForm.setValue(editingAddress);
    }



    /**
     * Saving the Address.
     * @returns 
     */  
    save(){
        this.submitted = true;
         // reset alerts on submit
        this.alertService.clear();
        console.log("f.address.hasError('minlength') ",this.f.address)

         // stop here if form is invalid
         if (this.addressForm?.invalid) {
             return;
         }
       
         var data = {};

        
        for (const field in this.addressForm.controls) { // 'field' is a string
           if(field!="addid")  //Skip Addid as it must be auto generated.
              data[field] = this.addressForm.controls[field].value;
        }
        data["userid"] = this.dataJson.user.id;

       // console.log("Address Data : ",JSON.stringify(data));
        

        this.addressService.register(JSON.stringify(data))
            .pipe(first())
            .subscribe(
                data => {
                    this.results = "Saved Successfully!";
                    this.alertService.success('Added Address successfully', true);
                    this.loadAllAddress();
                    for (const field in this.addressForm.controls) { // 'field' is a string
                        this.addressForm.controls[field].reset();
                        this.addressForm.controls[field].setErrors(null);
                    }
                    setTimeout(()=>{ 
                        this.alertService.clear();
                        this.results = null;
                        this. isEditAddNew = false;
                        this.methodName = null;
                        this.editAddButton = null;
                    }, 2000);
                    
                    
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }


       
  }
