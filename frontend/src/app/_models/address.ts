export interface Address {
    addid: number;
    userid: number;
    addtype: string;
    address: string;
    addressee: string;
    mobile: string;
    longitude: string;
    latitude: string;

}