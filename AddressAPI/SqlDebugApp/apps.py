from django.apps import AppConfig


class SqldebugappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'SqlDebugApp'
