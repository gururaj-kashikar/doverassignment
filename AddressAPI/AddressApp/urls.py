from django.urls import re_path, include
from AddressApp import views
from AddressApp.views import CustomObtainAuthToken
from rest_framework.authtoken.views import obtain_auth_token  # <-- Here
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)



urlpatterns=[
    re_path(r'^', include(router.urls)),
    re_path(r'^address/$', views.addressApi),
    re_path(r'^address/([0-9]+)$', views.addressApi),
    re_path(r'^auth/$',CustomObtainAuthToken.as_view()),
    re_path(r'^login/$', views.login_user),
    re_path(r'^logout/$', views.logout_user),
    re_path(r'api-token-auth/', obtain_auth_token, name='api_token_auth'),  # <-- And here

]