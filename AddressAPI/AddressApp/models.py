from django.db import models

class Users(models.Model):
    userid = models.AutoField(primary_key=True)
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    emailId = models.CharField(max_length=100) 
    password =  models.CharField(max_length=100) 
    def __str__(self):
        return "%s %s" % (self.FirstName, self.LastName)

class Address(models.Model): 
    addid = models.AutoField(primary_key=True)
    userid = models.CharField(max_length=100, null=False, blank=False)
    addressee = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    addtype = models.CharField(max_length=100)
    address = models.CharField(max_length=200)  
    longitude = models.DecimalField( max_digits=10, decimal_places=3, null=True, blank=True)
    latitude = models.DecimalField( max_digits=10, decimal_places=3, null=True, blank=True)
    