from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from AddressApp.models import Users, Address
from AddressApp.serializers import  AddressSerializer, UserSerializer
from django.contrib.auth.models import User
from http import HTTPStatus
from rest_framework import viewsets, status

from rest_framework.decorators import api_view, permission_classes

from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core import serializers

# ########################################################################
# Author : Gururaj
# Django's User authentication class
# This function is protected with IsAuthenticated permission class.
# ########################################################################   
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    model = User

    def get_permissions(self):
        # allow non-authenticated user to create
        return (AllowAny() if self.request.method == 'POST'
                else permissions.IsStaffOrTargetUser()),


@csrf_exempt
def login_user(request):
    if request.method == 'POST' :
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        Token.objects.create(user=instance)
        return JsonResponse(serializers.serialize('json', user), safe=False)
    else:
        # Return an 'invalid login' error message.
        return  JsonResponse("Invalid Username or Password", safe=False)

def logout_user(request):
    logout(request)
    return JsonResponse("Logged out Successfully", safe=False)

# ########################################################################
# Author : Gururaj
# GET, POST, PUT and Delete request for CRUD operation of Address.
# This function is protected with IsAuthenticated permission class.
# ########################################################################   

@api_view(['GET', 'POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])   
@csrf_exempt     
def addressApi(request, id=0):
    if request.method == 'GET' :
        address = Address.objects.filter(userid=id)
        address_serializer = AddressSerializer(address, many=True)
        return JsonResponse(address_serializer.data, safe=False)
    
    elif request.method == 'POST' :
        address_data = JSONParser().parse(request) 
        print('AddressAPI Post : ', address_data)
        address_serializer = AddressSerializer(data=address_data)
        if address_serializer.is_valid():
            address_serializer.save()
            return JsonResponse("Saved Address Sucessfully!", safe=False)
        else:
           return JsonResponse({'status':'false','message':"Invalid data. Please try with proper payload"}, safe=False, status=HTTPStatus.BAD_REQUEST)     
        return JsonResponse({'status':'false','message':"Could not save Address Data. Please try again."}, safe=False,  status=HTTP_500_INTERNAL_SERVER_ERROR)
   
    elif request.method == 'PUT' :
        new_address = JSONParser().parse(request) 
        address = Address.objects.get(addid=new_address['addid'])
        address_serializer=AddressSerializer(address, data=new_address, partial=True)
        valid =  address_serializer.is_valid(raise_exception=True)
        if valid:
            address_serializer.save()
            return  JsonResponse("Address Data Updated Sucessfully!", safe=False)
        else:
           return JsonResponse({'status':'false','message':"Invalid data. Please try with proper payload"}, safe=False, status=HTTPStatus.BAD_REQUEST)      
        return JsonResponse({'status':'false','message':"Could not update Address Data. Please try again."}, safe=False,  status=HTTP_500_INTERNAL_SERVER_ERROR)   

    elif request.method == 'DELETE' :
        print('Deleting Address of Id ', id)
        address = Address.objects.get(addid=id)
        address.delete()
        return  JsonResponse("Address Deleted Sucessfully!", safe=False)

########################################################################
# Author : Gururaj
# Custom athenticaiton class for obtaining token and creating JSON response.
# 
########################################################################  

class CustomObtainAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super(CustomObtainAuthToken, self).post(request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        user = User.objects.get(id=token.user_id)
        #login(request, user)
        serializer = UserSerializer(user, many=False)
        return JsonResponse({'token': token.key, 'user': serializer.data})