from rest_framework import serializers
from AddressApp.models import Users, Address
from django.contrib.auth.models import User, Group


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username','email', 'password','first_name','last_name', 'is_superuser', 'is_staff')
        extra_kwargs = {'password' : {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user        

class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'
        #fields = ('addid','userid',  'addressee', 'addtype', 'mobile', 'address',  'longitude','latitude')
        #extra_kwargs = {'addid': {'read_only': False}}   